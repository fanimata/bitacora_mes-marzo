# **Bitácora: Marzo**



## Práctica de Análisis transcriptómico 
---


#### *Martes 5 de marzo de 2019*



**Análisis de calidad de secuencias**


Para la práctica se utilizaron datos de secuenciación de *Schizosaccharomyces pombe*, los cuale fueron descargados en formato comprimido desde una carpeta de google drive, el comando utilizados para esto fue:

    $ tar -xvf /export/storage/nnb/t2/FastQC_Short.tar.gz

El archivo se descomprimió en una carpeta local y se revisó la carpeta descomprimida, usando el siguiente *script*:

    $ tar -xvf /home/fani/Documentos/I_Semestre_2019/Tesis/Tutorial_transcriptomica/FastQC_Short.tar.gz
    $ cd FastQC_Short
    $ ls

    Partial_SRR2467141.fastq 
    Partial_SRR2467142.fastq 
    Partial_SRR2467143.fastq 
    Partial_SRR2467144.fastq 
    Partial_SRR2467145.fastq 
    Partial_SRR2467146.fastq
    Partial_SRR2467147.fastq
    Partial_SRR2467148.fastq
    Partial_SRR2467149.fastq
    Partial_SRR2467150.fastq
    Partial_SRR2467151.fastq
    
Se revisó los archivos de secuenciación de Illumina usando el comando:

    $ head -n "archivo.fastq"

    NOTA: Los archivos de Illumina generalmente presentan el siguiente formato:
    <nombre de la muestra>_<secuencia identificadora (barcode)>_L<línea (3 dígitos)>_R<número de lectura (read)>_<número del set (3 dígitos)>.fastq.gz
    
Una vez revisada las secuencias se procedió a realizar el análisis de calidad con FastQC, para lo cual se creó una carpeta nueva:

    $ mkdir QUAL

Para poder correr el análisis de calidad de cada uno de los archivos en formato *"fastq"*, se aplicó un ciclo for a cada uno de estos dentro de la carpeta FastQC_Short:

    $ for i in $(ls *fastq); do echo $i; done
    
En donde cada archivo fue nombrado como "$i"

    $ fastQC -O /home/fani/Documentos/I_Semestre_2019/Tesis/Tutorial_transcriptoma/FastQC_Short/QUAL/ $i

En donde el argumento (*flag*) *-O* sifinica el directorio de salida.
Una vez finalizado el resultado se revió la carpeta QUAL, en donde se observó archivos con formato "html" y "zip":

    $ cd QUAL
    $ ls
    Partial_SRR2467151_fastqc.html
    Partial_SRR2467151_fastqc.zip

Se revisó los archivos "html" en el buscador y la siguiente información que despliega:

    1) Basic Statistics
    2) Per base sequence quality
    3) Per tile sequence quiality
    4) Per sequence quality scores
    5) Per base sequence content
    6) Per sequence GC content
    7) Per base N content
    8) Sequence Length Distribution
    9) Sequence Duplication Levels
    10) Overrrepresented sequences
    11) Adapter Content
    12) Kmer Content
    
Con todo esto se observó que la distribución de GC es muy similar a la distribución teórica, que hay pocas bases ambiguas, que todas las secuencias tienen el mismo tamaño, que hay pocas secuencias sobre representadas y otros.
Se descomprimió además los archivos en formato "zip" y se revisó las carpetas:
    
    $ unzip Partial_SRR2467151_fastqc.zip
    $ cd Partial_SRR2467151_fastqc
    $ ls
    
Ahí dentro se observó un reporte del análisis en texto plano, el cual se revisó con el siguiente comando:

    $ more summary.txt


**Limpieza de las secuencias**


Para llevar a cabo la limpieza de los archivos "fastq" se utilizó el programa *Trimmomatic* basado en Java, y posee opciones de filtrado para lecturas individuales o emparejadas (single-end y paired-end)
Se le pidió a *Trimmomatic* que recortara lo siguiente:

    1) Los adaptadores Illumina TruSeq single-end (SE) del extremo 3' 
    2) Los primeros 10 nucleótidos del extremos 5'
    3) Bases de mala calidad usando una ventana de 4 nulceótidos con un promedio mínimo de Phred de 15
    4) Secuencias de longitud menor a 60 nucleótidos
    
Antes de correr el programa se descargó el archivo de los adaptadores Tru-Seq SE en la carpeta FastQC_Short:

    $ wget https://raw.githubusercontent.com/timflutre/trimmomatic/master/adapters/TruSeq3-SE.fa ./

Luego se procedió a correr *Trimmomatic* usando el siguiente *script*:

    $ for i in $(ls *fastq); do echo $i; done
    $ trimmomatic SE $i Trimmed_$i ILLUMINACLIP:TruSeq3-SE.fa:2:30:10 HEADCROP:10 SLIDINGWINDOW:4:15 MINLEN:60

Una vez finalizado el análisis se revisó la carpeta y los archivos generados:

    $ ls
    $ head -n 12 Trimmed_Partial_SRR2467151.fastq
    
Una vez recortadas las secuencias "fastq" se corrió nuevamente un análisis de calidad usando FastQC:

    $ for i in $(ls Trimmed*); do echo $i; done
    $ fastqc -O ./QUAL/ $i
    
Se revisó los archivos "html" generados en este nuevo análisis y se comparó la calidad de las seuencias antes y después de la corrida con *Trimmomatic*.



#### *Jueves 7 de marzo de 2019*


